Picard MarkDuplicates GeneFlow App
==================================

Version: 2.22.7-01

This GeneFlow app wraps the Picard MarkDuplicates tool.

Inputs
------

1. input: BAM File - An alignment BAM file, sorted by coordinate.

Parameters
----------

1. remove_duplicates: Remove duplicate reads, default: false

2. validation_stringency: Stringency of Validation (for SAM), default: lenient

3. assume_sort_order: Sort order of input, default: coordinate

4. output: Output directory - The name of the output directory, where the BAM and metrics files will be stored. Default: output

